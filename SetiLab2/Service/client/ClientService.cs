﻿using SetiLab2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.Service.client
{
    class ClientService
    {
        UserDAO userDAO;
        BillDAO billDAO;

        public ClientService(UserDAO uDAO, BillDAO bDAO)
        {
            userDAO = uDAO;
            billDAO = bDAO;

        }

         public string Trans(string sumStr, string sender, string recipient)
        {
            string IdSender = "";
            string IdRecipient = "";
            string BillIdSender = "";
            string BillIdRecipient = "";
            string amount = "";
            int sumInt = Int32.Parse(sumStr);
            IdSender = userDAO.GetIdByLogin(sender);

            IdRecipient = userDAO.GetIdByLogin(recipient);

            //Проверки на существование счетов у выбранных пользователей
            if ((BillIdSender = billDAO.GetBillByClientId(IdSender)) == "error")
            {
                return "error";
             
            }

            if ((BillIdRecipient = billDAO.GetBillByClientId(IdRecipient)) == "error")
            {
                return "error";
              
            }
            amount = billDAO.GetAmountByClientID(IdSender);
            string amountR = billDAO.GetAmountByClientID(IdRecipient);
            //Проверка на достаточность баланса
            int balanceSender = Int32.Parse(amount) - Int32.Parse(sumStr);
            int balanceR = Int32.Parse(amountR) + Int32.Parse(sumStr);
            if (Int32.Parse(amount) < Int32.Parse(sumStr))
            {
                return "error";
                
            }

            billDAO.NewBalanceClinet(IdSender, balanceSender.ToString());
            billDAO.NewBalanceClinet(IdRecipient, balanceR.ToString());

            return userDAO.Transfer(sumInt, sender, recipient);
        }

        internal string GetIdByLogin(string login)
        {
            return userDAO.GetIdByLogin(login);
        }

        internal string GetAmountByClientID(string idLogin)
        {
            return billDAO.GetAmountByClientID(idLogin);
        }

        internal string GetBillByClientId(string idSender)
        {
            return billDAO.GetBillByClientId(idSender);
        }

        internal void NewBalanceClinet(string idSender, string v)
        {
            billDAO.NewBalanceClinet(idSender, v);
;        }

        internal string UserTransfer(string sender)
        {
            return userDAO.UserTransfer(sender);
        }

        internal string Transfer(int sumInt, string sender, string recipient)
        {
            return userDAO.Transfer(sumInt, sender, recipient);
        }
    }
}
