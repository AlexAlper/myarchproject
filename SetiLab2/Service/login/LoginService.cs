﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetiLab2.DAO;
using SetiLab2.Model;

namespace SetiLab2.Service.login
{
    class LoginService: ILoginService
    {

        UserDAO userDAO;

        public LoginService(UserDAO aDAO)
        {
            userDAO = aDAO;
        }

        public User Login(string login, string pass )
        {
           
          User user =  userDAO.Login(login, pass);



            return user;
      
     

        }

        
    }
}