﻿using SetiLab2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.Service.login
{
    public interface ILoginService
    {
        User Login(string login, string pass);
    }
}
