﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.Model
{
   public class User
    {
       public string id;
       public string login;
        public string pass;
        public string name;
        public string surname;
        public string salary;

        public User(){}

        public User(string _id, string _login, string _pass, string _name, string _surname, string _salary)
        {
            id = _id;
            login = _login;
            pass = _pass;
            name = _name;
            surname = _surname;
            salary = _salary;
        }  
    }
}
