﻿using SetiLab2.Controller;
using SetiLab2.Model;
using SetiLab2.Service.login;
using SetiLab2.views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SetiLab2
{
    class LoginController: IController
    {

        //UserModel _userModel;

        private LoginService _loginService;

        private IView view;

       public  LoginController(IView aView, LoginService loginService)
        {
            view = aView;
            view.EventLoginUser += Login;
            _loginService = loginService;

           // _userModel = userModel;
        }
        public void Login(object aut, EventArgs e) //добавить label
        {
            try
            {
                AutUser a = (AutUser)aut;
                User user = new User();
               user = _loginService.Login(a.login, a.pass);
          

                if (user.id != null)
                {
                    view.LoginUser(user.login, user.id);
                    return;
                }
                throw new Exception("Вход не удался");
            }

            catch (Exception ex)
            {
                view.ShowMessageError(ex.Message);
            }
           // return _userModel.Login(login, password);
        }

        public void Run()
        {
            view.Show();
        }
    }
}
