﻿using SetiLab2.Model;
using SetiLab2.Service.client;
using SetiLab2.Service.login;
using SetiLab2.views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2
{
    class ClientController
    {

        private ClientService _loginService;

        private IViewClient view;

        public ClientController(IViewClient aView, ClientService loginService)
        {
            view = aView;
           view.EventGetAmount += GetAmount;
            view.EventUserTransfer += UserTransfer;
            view.EventTransfer += Transfer;
            _loginService = loginService;

        }

        public void GetAmount(object aut, EventArgs e)
        {
            string login = (string)aut;
           String IdLogin = _loginService.GetIdByLogin(login);
            view.ShowAmount(_loginService.GetAmountByClientID(IdLogin));
          // return _loginService.GetAmountByClientID(IdLogin);
        }


        public void Transfer(object aut, EventArgs e) 
        {
            string str = (string)aut;
            String[] words = str.Split(new char[] { ' ' });

            string sumStr = words[0];
            string sender = words[1];
            string recipient = words[2];
            int sumInt;
          


            //Проверка на корректность входных данных
            try
            {
                sumInt = Int32.Parse(sumStr);
                if (sumInt < 0)
                {
                    view.ShowMessageError("Значение перевода может быть только положительным (>0)");
                    return;
                    
                }
                if (recipient == "")
                {
                    view.ShowMessageError("Получатель не задан");
                    return;
                    
                }
            }
            catch
            {
                view.ShowMessageError("Баланс может быть только целочисленным числом");
                return;
                
            }

            view.ShowMessageError(_loginService.Trans(sumStr, sender, recipient));
        }

        public void UserTransfer(object aut, EventArgs e)
        {
            string sender = (string)aut;
            view.ShowUserTransfer(_loginService.UserTransfer(sender));
        }

        public void Run()
        {
            view.Show();
        }


    }
}
