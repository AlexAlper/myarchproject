﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.DAO
{
     

    abstract class DAO<T>
    {
        private String TableName;
        public BD bd = new BD();

       


        public NpgsqlDataReader GetSelect(string command)
        {
            bd = new BD();
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand(command, bd.conn);
            NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
            return npgSqlDataReader;

        }
    }
}
