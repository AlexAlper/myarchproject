﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.views
{
    public interface IView
    {
        void ShowMessageError(string msg);
   
        void LoginUser(string login, string id);

        event EventHandler<EventArgs> EventLoginUser;

        void Show();
    }
}
