﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetiLab2.views
{
    public interface IViewClient
    {
        void ShowMessageError(string msg);

        void ShowAmount(string msg);

        void ShowUserTransfer(string msg);

        event EventHandler<EventArgs> EventGetAmount;

        event EventHandler<EventArgs> EventTransfer;

        event EventHandler<EventArgs> EventUserTransfer;

        void Show();

    }
}
