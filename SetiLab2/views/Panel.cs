﻿using Npgsql;
using SetiLab2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetiLab2.views;
using SetiLab2.Service.client;
using Ninject;
using System.Reflection;

namespace SetiLab2
{


    public partial class Panel : Form, IViewClient
    {
        //ClientController clientController = new ClientController(;
        string name;
        string id;

        public event EventHandler<EventArgs> EventGetAmount;
        public event EventHandler<EventArgs> EventTransfer;
        public event EventHandler<EventArgs> EventUserTransfer;

        //UserModel UserModel;
        //BillModel billModel;
        ClientController clientController;
        ClientService clientService;

        public Panel()
        {
            InitializeComponent();


           //  IKernel kernel = new StandardKernel();
            //  new NinjectDI(kernel).Load();
            // kernel.Load(Assembly.GetExecutingAssembly());
           // Panel game = new Panel();

            //game.ShowDialog();
             
            //UserModel = new UserModel();
            //billModel = new BillModel();
            //clientController = new ClientController(UserModel, billModel);
            this.name = "alex";
            this.id = "1";
            EventArgs e = new EventArgs();
            EventGetAmount?.Invoke(name, e);
          
             label4.Text = name;

            EventUserTransfer?.Invoke(name, e);
           // comboBox1.Items.Add(clientController.UserTransfer(name));
          
    
        }

        private void Panel_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EventTransfer?.Invoke(textBox1.Text + " " + name + " " + comboBox1.Text, e);

            // label8.Text = clientController.Transfer(textBox1.Text, name, comboBox1.Text);
            EventGetAmount?.Invoke(name, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void ShowMessageError(string msg)
        {
            label8.Text = msg;
        }

        public void ShowAmount(string msg)
        {
            label2.Text = msg;
        }

        public void ShowUserTransfer(string msg)
        {
            comboBox1.Items.Add(msg);
        }

        public new void Show()
        {
            Application.Run(this);
        }
    }
}
