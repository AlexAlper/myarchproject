﻿using Ninject;
using Ninject.Modules;
using SetiLab2.Controller;
using SetiLab2.Model;
using SetiLab2.Service.client;
using SetiLab2.Service.login;
using SetiLab2.views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SetiLab2
{

    public class Bindings : NinjectModule
    {
        public override void Load()
        {

            Bind<IView>().To<Login>().InSingletonScope();
            Bind<IViewClient>().To<Panel>().InSingletonScope();
            Bind<BillDAO>().To<BillDAO>().InSingletonScope();
            Bind<UserDAO>().To<UserDAO>().InSingletonScope();
            Bind<ClientController>().To<ClientController>();
            Bind<ClientService>().To<ClientService>();

            Bind<LoginService>().To<LoginService>().InSingletonScope();
            Bind<LoginController>().To<LoginController>().InSingletonScope();
            Bind<BD>().To<BD>().InSingletonScope();

        }
    }

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {

            IKernel kernel = new StandardKernel();
          //  new NinjectDI(kernel).Load();
           kernel.Load(Assembly.GetExecutingAssembly());
       

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoginController loginController  = kernel.Get<LoginController>();
            ClientController clientController = kernel.Get<ClientController>();

            loginController.Run();
        }
    }
}
